## [Task Description Here](https://drive.google.com/file/d/1IFnaVLv9qzssBWVAQSpFkNEzYHzFrT8u/view?usp=sharing)

## Usage Instructions

### Python 3 or higher should be used.

### Create a virtual environment and install dependencies.

`pip install -r requirements.txt`

It just contains dependencies to fetch data from Google Directory API.


### Activate virtual environment and run the test cases using:
`python -m unittest`


### All the tasks are defined in `task.py` in root folder.
It can be imported and run idependently.
```
from main.task import (
    built_graph,
    get_resouce_hierarchy,
    get_identity_roles,
    get_identity_and_role_by_resource,
    fetch_data_from_google_api,
    get_user_identity_roles,
    get_id_and_role_by_resource
)
```

Check function definition symbol to know what argument the function takes.