from __future__ import print_function
from os import path
from typing import Any, Dict, Optional, Sequence
from googleapiclient.discovery import build  # type: ignore
from oauth2client.service_account import ServiceAccountCredentials  # type: ignore
from config import PROJECT_ROOT


SERVICE_ACCOUNT_FILE = path.join(
    PROJECT_ROOT,
    'secrets.json'
)


def get_service(scopes: Optional[Sequence[str]] = []):
    """Returns google builded service.

    Args:
        scopes: List of scopes
    """

    credentials = ServiceAccountCredentials.from_json_keyfile_name(  # type: ignore
        SERVICE_ACCOUNT_FILE,
        scopes=scopes
    )

    credentials = credentials.create_delegated( # type: ignore
        "ron@test.authomize.com")  # type: ignore

    service = build('admin', 'directory_v1', credentials=credentials)

    return service


def get_directory_users() -> Sequence[str]:
    """Gets all users from directory

    Returns:
       list of users string
    """

    scopes = ['https://www.googleapis.com/auth/admin.directory.user.readonly']

    service = get_service(scopes)

    results = service.users().list(  # type: ignore
        orderBy='email',
        customer='my_customer'
    ).execute()

    users = results.get('users', [])  # type: ignore

    return [
        f"{user['primaryEmail']}"
        f"({user['name']['fullName']})"

        for user in users  # type: ignore
    ]


def get_directory_groups() -> Sequence[str]:
    """Get all groups from directory

    Returns:
        All group strings in a directory
    """

    scopes = ['https://www.googleapis.com/auth/admin.directory.group.readonly']

    service = get_service(scopes)

    results = service.groups().list(  # type: ignore
        customer='my_customer'
    ).execute()

    groups = results.get('groups', [])  # type: ignore

    return [  # type: ignore
        group['email']
        for group in groups  # type: ignore
    ]


def get_all_group_users() -> Dict[str, Sequence[str]]:
    """Gets all the groups and its users

    Returns:
        Dict of all its groups and its users.
    """
    scopes = ['https://www.googleapis.com/auth/admin.directory.group.readonly']

    output: Dict[str, Sequence[str]] = dict()

    service = get_service(scopes)

    groups = get_directory_groups()

    for group_name in groups:
        results = service.members().list(  # type: ignore
            groupKey=group_name
        ).execute()

        members: Any = results.get('members', [])  # type: ignore

        output[group_name] = [
            member['email']
            for member in members
        ]

    return output
