
import json

from typing import Any, Dict, Sequence, TextIO, cast


from models.identity import (
    Identity,
    UserIdentity,
    ServiceAccountIdentity,
    GroupIdentity,
    GroupIdentityMember,
)

from models.resource import (
    ParentResourceType,
    Resource,
    ResourceIdentity,
    ResourceIdentityRole
)

USER, SERVICE_ACCOUNT, GROUP, DOMAIN = (
    'user', 'serviceAccount', 'group', 'domain')


def get_resource(ancestors: Sequence[str]) -> Resource:
    '''Returns resource object from  ancestors in 
    json line creating its parent resources

    Args:
        ancestors: List of ancestors.

    Returns:
        Resource instance
    '''

    parent_resource: ParentResourceType = None

    for ancestor_resource_unique_id in ancestors[::-1]:

        parent_resource = Resource.get_or_create(
                ancestor_resource_unique_id,
                parent_resource=parent_resource
            )

    return cast(Resource, parent_resource)


def make_identity(role_identity: str) -> Identity:
    """Returns and Identity instance from raw identity data

    Args:
        role_identity: Raw identity data

    Returns:
        Identity instance regardless of its type.
    """

    if role_identity.startswith(USER):
        return cast(
            Identity, UserIdentity.get_or_create(
                role_identity
            )
        )

    elif role_identity.startswith(SERVICE_ACCOUNT):
        return cast(
            Identity, ServiceAccountIdentity.get_or_create(
                role_identity
            )
        )

    elif role_identity.startswith(GROUP):
        return cast(
            Identity, GroupIdentity.get_or_create(
                role_identity
            )
        )

    else:
        return Identity.get_or_create(
                role_identity
            )

def make_resource_identity(resource: Resource, identity: Identity) -> ResourceIdentity:
    """Returns resource identity

    Args:
        resource: Resource instance
        identity: Identity instance

    Returns:
        ResourceIdentity instance
    """

    return ResourceIdentity.get_or_create(
            resource,
            identity
        )


def make_resouce_user_role(
    resource: Resource, role_identity: str, role_constant: str
) -> None:

    """Make a role for resource and identity from the raw data

    Args:
        resource: Resource instance
        role_identity: raw data for identity
        role_constant: string represnting role
    """

    identity = make_identity(role_identity)

    
    resource_identity = make_resource_identity(
        resource,
        identity
    )

    ResourceIdentityRole.get_or_create(
        resource_identity,
        role_constant
    )


def handle_bindings(resource: Resource, bindings: Sequence[Dict[str, Any]]) -> None:
    """ Handles all the role binding data present in raw data

    Args:
        resource: Resource instance 
        bindings: Raw resource role binding data
    """

    for binding in bindings:
        role_constant: str = binding['role']
        role_identities: Sequence[str] = binding['members']

        for role_identity in role_identities:
            make_resouce_user_role(
                resource,
                role_identity,
                role_constant
            )


def parse_raw_resource(resource_dict: Dict[str, Any]) -> None:
    """ This function make a resource from sample data, extracts roles binding
    raw data and sends it for further processing.

    Args:
        resource_dict: A raw dict consisting resource information and role bindings 
            associated with it.
        
    """
    resource = get_resource(
        resource_dict['ancestors']
    )

    bindings: Sequence[Dict[str, Any]
                       ] = resource_dict['iam_policy']['bindings']

    handle_bindings(resource, bindings)


def build_graph_from_sample(data_file: str) -> None:
    """This function gets a JSON line from a sample data and sends
    it for parsing.

    Args:
       data_file: absolute path of a sample file
    """

    reader: TextIO = open(data_file, 'r')
    content = reader.readlines()

    for line in content:
        resource_dict = json.loads(line)

        parse_raw_resource(resource_dict)

    reader.close()


def add_group_users_to_graph(user_groups: Dict[str, Sequence[str]]) -> bool:
    """This method add groups and its users from raw data to graph.
    
    Args:
        user_groups: Raw data
    """

    for group_unique_id in user_groups:
        group_identity = GroupIdentity.get_or_create(
            f"{GROUP}:{group_unique_id}"
        )

        for member_unique_id in user_groups[group_unique_id]:

            member = UserIdentity.get_or_create(
                f"{USER}:{member_unique_id}"
            )

            GroupIdentityMember.get_or_create(
                group_identity,
                member
            )

    return True
