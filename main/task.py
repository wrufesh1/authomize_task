from models.identity import IdentityUID
from models.resource import ResourceRoleConstant, ResourceUID
from models.commons import BaseModelUID
from os import path
from typing import Any, Sequence

from .parser import build_graph_from_sample
from config import PROJECT_ROOT

from models import (
    Resource,
    Identity,
    UserIdentity
)

from .google_api_utils import (
    get_directory_users,
    get_directory_groups,
    get_all_group_users
)


DATA_SAMPLE_FILE = path.join(
    PROJECT_ROOT,
    'data_sample',
    'roles_sample_file.json'
)


def built_graph() -> None:
    """Task 1: Builds graph form sample data
    """
    build_graph_from_sample(DATA_SAMPLE_FILE)


# Task 2
def get_resouce_hierarchy(resource_unique_id: ResourceUID) -> Sequence[ResourceUID]:
    """Task 2: Gets parent resources from resource

    Args:
        resource_unique_id: Unique ID of a resource.

    Returns:
        List of parent resource ids in order.
    """

    hierarchy: Sequence[str] = []

    resource = Resource.get(resource_unique_id)

    while resource.parent_resource != None:
        hierarchy.append(
            resource.parent_resource.unique_id
        )

        resource = resource.parent_resource

    return hierarchy

# Task 3
def get_identity_roles(identity_unique_id: IdentityUID) -> Sequence[
    tuple[BaseModelUID, str, ResourceRoleConstant]]:
    """Task 3: Gets all the role of identity

    Args:
        identity_unique_id: Identity unique ID

    Returns:
        List of strings representing identity roles to resources.
    """

    identity = Identity.get(identity_unique_id)

    roles = [
        (
            role.resource_identity.resource.unique_id,
            role.resource_identity.resource.asset_type,
            role.role_constant
        ) for role in identity.resource_identity_roles.values()
    ]

    return roles


def get_identity_and_role_by_resource(
    resource_unique_id: ResourceUID) -> Sequence[
    tuple[BaseModelUID, ResourceRoleConstant]]:
    """Task 4: Get identity roles by resource

    Args:
        resource_unique_id: Unique ID of resource
    
    Returns:
        List of strings representing identity roles to resources.
    """

    result: Sequence[tuple[BaseModelUID, ResourceRoleConstant]] = []

    resource = Resource.get(resource_unique_id)

    for resouce_identity in resource.resource_identities.values():
        for resource_identity_role in resouce_identity.roles.values():

            result.append(
                (
                    resouce_identity.identity.unique_id,
                    resource_identity_role.role_constant
                )
            )

    return result


# Task 5

def fetch_data_from_google_api() -> Any:
    """Task 4: Fetches data from google api
    """
    users = get_directory_users()
    groups = get_directory_groups()
    group_users = get_all_group_users()

    return (
        users,
        groups,
        group_users
    )


# Task6
def get_user_identity_roles(user_identity_unique_id: BaseModelUID) -> Sequence[
    tuple[BaseModelUID, str, ResourceRoleConstant]] :
    """Task 6: Same as Task 3 because group level resource are cloned to user level
    via post create tasks.

    => class: Resource => method: inherit_parent_resource_roles

    => class: ResourceIdentityRole => method: inherit_role_in_child_resources
    => class: ResourceIdentityRole => method: inherit_role_group_member_identities

    => class: GroupIdentityMember => method: add_resource_identity_roles

    Args:
        identity_unique_id: Identity unique ID

    Returns:
        List of strings representing identity roles to resources.
    """

    user_identity = UserIdentity.get(user_identity_unique_id)

    roles = [
        (
            role.resource_identity.resource.unique_id,
            role.resource_identity.resource.asset_type,
            role.role_constant
        ) for role in user_identity.resource_identity_roles.values()
    ]

    return roles


def get_id_and_role_by_resource(resource_unique_id: ResourceUID) -> Sequence[
    tuple[BaseModelUID, ResourceRoleConstant]]:
    """ Task 6: Same as Task 4 because group level resource are cloned to user level
    via post create task as follow

    => class: Resource => method: inherit_parent_resource_roles

    => class: ResourceIdentityRole => method: inherit_role_in_child_resources
    => class: ResourceIdentityRole => method: inherit_role_group_member_identities

    => class: GroupIdentityMember => method: add_resource_identity_roles

    Args:
        resource_unique_id: Unique ID of resource
    
    Returns:
        List of strings representing identity roles to resources.
    """

    result: Sequence[
    tuple[BaseModelUID, ResourceRoleConstant]] = []

    resource = Resource.get(resource_unique_id)

    for resouce_identity in resource.resource_identities.values():
        for resource_identity_role in resouce_identity.roles.values():

            result.append(
                (
                    resouce_identity.identity.unique_id,
                    resource_identity_role.role_constant
                )
            )

    return result
