from typing import (
    Any,
    Type,
    TypeVar
)


from collections.abc import (
    Sequence,
    MutableMapping
)

from .root_graph import graph

BaseModelUID = str

CollectionNameType = str


DerivedClass = TypeVar('DerivedClass', bound='BaseModel')


class BaseModel(object):
    """Base class for models

    Args:
        *args: Implemented class variable arguments.
        
        **kwargs: Implemented class keyword arguments

    Attributes:
        _unique_id: Private property holds unique_id of an object of BaseModel.
        
        unique_id: This is unique id of object of BaseModel.

        collection_names: Class attribute holds the collection names in which
            the implemented class instance will be added to in root node of 
            graph

    
    """

    def __init__(self, *args: Any, **kwargs:Any) -> None:
        ...

    collection_names: Sequence[CollectionNameType]

    _unique_id: BaseModelUID
    
    @property
    def unique_id(self) -> BaseModelUID:
        return self._unique_id

    def post_create_task(self) -> None: ...
 

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, type(self)) and (
            self.unique_id == other.unique_id
        )

    def __ne__(self, other: Any) -> bool:
        return not (isinstance(other, type(self)) and (
            self.unique_id == other.unique_id
        ))

    @classmethod
    def get(
        cls: Type[DerivedClass], unique_id: BaseModelUID
    ) -> DerivedClass:
        """Gets implemented class instance from the collection 
        of implemented class unique objects in the root node of graph.

        Args:
            unique_id: unique_id of implemented class object

        Raises:
            KeyError: If the key is not found in the collection
        """

        default_collection_value: MutableMapping[BaseModelUID, DerivedClass] = dict()

        
        collection: MutableMapping[BaseModelUID, DerivedClass] = getattr(
            graph,
            cls.collection_names[-1],
            default_collection_value
        )
        return collection[unique_id]

    @classmethod
    def get_or_create(
        cls: Type[DerivedClass], *args: Any, **kwargs: Any
    ) -> DerivedClass:
    
        """Creates a new instance when the reference in the collection
        is not found otherwise returns instance from the collection.

        Args:
            
            args: Variable arguments the BaseClass 
                implemented class __init__ takes
            
            kwargs: Keyword arguments the BaseClass implemented 
                class __init__ takes

        
        Returns:
            Implemented class instance

        """


        instance_obj: DerivedClass = cls(*args, **kwargs)

        
        for collection_name in cls.collection_names:

            new_mapping_obj: MutableMapping[BaseModelUID, DerivedClass] = dict()

            if not hasattr(graph, collection_name):
                setattr(
                    graph,
                    collection_name,
                    new_mapping_obj
                )

            collection: MutableMapping[BaseModelUID, DerivedClass] = getattr(
                graph,
                collection_name
            )

            instance = cls.add_instance_to_collection(
                collection,
                instance_obj
            )

            instance_obj = instance

        
        instance_obj.post_create_task()


        return instance_obj

    @classmethod
    def add_instance_to_collection(
        cls: Type[DerivedClass],
        collection: MutableMapping[BaseModelUID, DerivedClass],
        instance_obj: DerivedClass
    ) -> DerivedClass:
        """Fetch or insert instance object in collection
        
        Args:
            collection: collection of instances
            instance_obj: instance to be fetched or inserted into
                collection

        Returns:
            fetched or inserted instance
        """

        instance = collection.setdefault(
                instance_obj.unique_id,
                instance_obj
            )
        return instance