from typing import TYPE_CHECKING, Union

from collections.abc import (
    MutableMapping
)
from .commons import BaseModel

if TYPE_CHECKING:
    from .resource import (
        ResourceIdentity,
        ResourceIdentityRole,
        ResourceIdentityUID,
        ResourceIdentityRoleUID,
    )


IdentityUID = str
UserIdentityUID = str
ServiceAccountUID = str
GroupIdentityUID = str
GroupIdentityMemberUID = str


class Identity(BaseModel):
    """This models represents all kinds of identitities

    Args:
        unique_id: Unique id of identity

    Attributes:
        _unique_id: Private property holds unique_id of an object of Identity.

        unique_id: This is unique id of object of BaseModel.

        collection_names: Class attribute holds the collection names in which
            the Identity instance will be added to in root node of 
            graph

        resource_identities: Holds resources to which this Identity holds

        resource_identity_roles: Holes roles of resources which this 
            Identity holds.
    """

    collection_names = ('identities',)

    def __init__(self, unique_id: IdentityUID):

        assert unique_id, "'unique_id' property is required"

        self._unique_id: IdentityUID = unique_id

        self.resource_identities: MutableMapping[
            ResourceIdentityUID,
            ResourceIdentity
        ] = dict()

        self.resource_identity_roles: MutableMapping[
            ResourceIdentityRoleUID,
            ResourceIdentityRole
        ] = dict()

    def add_resource_identity(self, resource_identity: 'ResourceIdentity'):
        """This methods adds resource to this Identity via ResourceIdentity

        Args:
             resource_identity: ResourceIdentity object to add
        """

        if not resource_identity.identity == self:
            raise ValueError("Invalid 'resource_identity'")

        self.resource_identities.setdefault(
            resource_identity.unique_id,
            resource_identity
        )

    def add_resource_identity_role(self, resource_identity_role: 'ResourceIdentityRole'):
        """This methods adds resource's role to this Identity via ResourceIdentityRole

        Args:
             resource_identity_role: ResourceIdentityRole object to add
        """

        if not resource_identity_role.resource_identity.identity == self:
            raise ValueError("Invalid 'resource_identity_role'")

        self.resource_identity_roles.setdefault(
            resource_identity_role.unique_id,
            resource_identity_role
        )


GroupIdentityMemberType = Union[
    'UserIdentity',
    'ServiceAccountIdentity'
]



class ServiceAccountAndUserIdentityMixin(object):
    """This mixin is to applied to the types of odentities which 
    can be added to the GroupIdentity.

    Attributes:
        member_group_identities: This gives the collection of GroupIdentityMembers
           with the help of which we can find in what GroupIdentity the self Identity are 
           being added.
        
    """

    member_group_identities: MutableMapping[
        GroupIdentityMemberUID,
        'GroupIdentityMember'
    ] = dict()

    def add_member_group_identities(
        self, group_identity_member: 'GroupIdentityMember'
    ):
        """Adds GroupIdentityMember to member_group_identities 
        collection

        Args:
            group_identity_member: GroupIdentityMember to add
        """
        if not group_identity_member.member == self:
            raise ValueError("Invalid group_identity")

        self.member_group_identities.setdefault(
            group_identity_member.unique_id,
            group_identity_member
        )


class UserIdentity(Identity, ServiceAccountAndUserIdentityMixin):

    collection_names = ('identities', 'user_identities')


class ServiceAccountIdentity(Identity, ServiceAccountAndUserIdentityMixin):

    collection_names = ('identities', 'service_account_identities')


class GroupIdentity(Identity):
    """The type of identity which can hold the collection of 
    other type identities and when a resource to a role is applied 
    to it, it gets applies to all other types of identities it holds.

    Atrributes:
        group_identity_members: Collection of its members via GroupIdentityMember. 
    """

    collection_names = ('identities', 'group_identities')

    def __init__(self, unique_id: GroupIdentityUID) -> None:
        super().__init__(unique_id)

        self.group_identity_members: MutableMapping[
            GroupIdentityMemberUID,
            GroupIdentityMember
        ] = dict()

    def add_group_identity_member(self, group_identity_member: 'GroupIdentityMember'):
        """Adds GroupIdentityMember instance to group_identity_members attribute of 
        GroupIdentity.

        Args:
            group_identity_member: GroupIdentityMember instance to add to group_identity_members 
            attribute of GroupIdentity.
        """
        if not group_identity_member.group_identity == self:
            raise ValueError('Invalid member')

        self.group_identity_members.setdefault(
            group_identity_member.unique_id,
            group_identity_member
        )



class GroupIdentityMember(BaseModel):
    """Class representing members of GroupIdentity instance.

    Attributes:
        group_identity: GroupIdentity instance for which a member is to be defined.
        member: Other type of identity to be added as member.
    """

    collection_names = ('group_identity_members',)


    def __init__(self, group_identity: GroupIdentity, member: GroupIdentityMemberType):

        self.group_identity = group_identity
        self.member: GroupIdentityMemberType = member
        self._unique_id = f"{self.group_identity.unique_id}_{self.member.unique_id}"

    def post_create_task(self) -> None:
        """Post create task of GroupIdentityMember
        """
        self.group_identity.add_group_identity_member(self)
        self.member.add_member_group_identities(self)
        self.add_roles_to_group_identity_members()

    
    def add_roles_to_group_identity_members(self):
        """This method gets called as a part of post create task.
        It finds all the roles of group_identity and assigns its to a member 
        being used to create this instance.
        """

        from .resource import (
            ResourceIdentityRole,
            ResourceIdentity
        )

        group_identity_resource_roles = self.group_identity.resource_identity_roles.values()


        for group_identity_resource_role in group_identity_resource_roles:

            resource_identity = ResourceIdentity.get_or_create(
                    group_identity_resource_role.resource_identity.resource,
                    self.member
                )

            resource_identity_role = ResourceIdentityRole.get_or_create(
                    resource_identity,
                    group_identity_resource_role.role_constant,
                    assigned_explicitly=False
                )

            resource_identity_role.add_source_group_identity(
                self.group_identity
            )
