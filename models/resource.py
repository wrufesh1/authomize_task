from typing import Mapping, Optional, Union, cast

from collections.abc import (
    MutableMapping
)

from .commons import BaseModel, BaseModelUID, DerivedClass
from .identity import GroupIdentity, GroupIdentityUID, Identity


ResourceUID = str

ResourceIdentityUID = str

ResourceIdentityRoleUID = str

ResourceRoleConstant = str

ParentResourceType = Union[None, 'Resource']


class Resource(BaseModel):
    """This model resembles the resource.

    Attribites:
        asset_type: Type of resource.
        parent_resource: Parent resource
        child_resources: Child resources
        resource_identities: Identities the resource have access to via
            ResourceIdendity
    """

    collection_names = ('resources',)

    def __init__(
        self,
        unique_id: ResourceUID,
        parent_resource: Optional['ParentResourceType'] = None
    ):
        assert unique_id, (
            "'unique_id' property is required"
        )

        self._unique_id = unique_id
        self.asset_type: str = unique_id.split('/')[0]
        self.parent_resource: ParentResourceType = parent_resource

        self.child_resources: MutableMapping[ResourceUID, 'Resource'] = dict()

        self.resource_identities: MutableMapping[
            ResourceIdentityUID,
            'ResourceIdentity'] = dict()

    def post_create_task(self) -> None:
        """Rosource model post_create_task.
        """
        if self.parent_resource:
            self.parent_resource.add_child_resource(self)

        self.inherit_parent_resource_roles()

    def add_resouce_identity(self, resource_identity: 'ResourceIdentity'):
        """Adds identity that have access to a resource via ResourceIdentity

        Args:
            resource_identity: ResourceIdentity instance
        """
        if not resource_identity.resource == self:
            raise ValueError('Invalid resource identity')

        self.resource_identities.setdefault(
            resource_identity.unique_id,
            resource_identity
        )

    def add_child_resource(self, resource: 'Resource'):
        """Adds child resource to a resource.

        Args:
            resource: Resource instance
        """

        if not resource.parent_resource == self:
            raise ValueError('Invalid parent resource')

        self.child_resources.setdefault(
            resource.unique_id,
            resource
        )

    def inherit_parent_resource_roles(self):
        """This method is part of post create_task. And when called it
        inherits parent resource roles as new resource is created.
        """

        if self.parent_resource:
            for resource_identity in self.parent_resource.resource_identities.values():
                for role in resource_identity.roles.values():

                    new_role_resouce_identity = ResourceIdentity.get_or_create(
                        self,
                        resource_identity.identity
                    )

                    resource_identity_role = ResourceIdentityRole.get_or_create(
                        new_role_resouce_identity,
                        role.role_constant,
                        assigned_explicitly=False
                    )

                    resource_identity_role.add_inherited_from_resource_identity(
                        new_role_resouce_identity
                    )


class ResourceIdentity(BaseModel):
    """This models holds the relation of resource and identity.

    Attributes:
        resource: Resource instance
        identity: Identity instance
        roles: Collection of ResourceIdentityRole instances
    """

    collection_names = ('resource_identities',)

    def __init__(self, resource: Resource, identity: Identity):

        assert resource and identity, (
            "Both 'identity' and 'resource' properties are required"
        )

        self.resource: Resource = resource
        self.identity: Identity = identity

        self._unique_id = f"{self.resource.unique_id}_{self.identity.unique_id}"

        self.roles: MutableMapping[ResourceIdentityRoleUID,
                                   'ResourceIdentityRole'] = dict()

    def add_role(self, resource_identity_role: 'ResourceIdentityRole'):
        """Adds roles to ResourceIdentity instance via ResourceIdentityRole

        Args:
            resource_identity_role: ResourceIdentityRole Instance
        """

        if not resource_identity_role.resource_identity == self:
            raise ValueError('Not valid resource_identity_role.')

        self.roles.setdefault(
            resource_identity_role.unique_id,
            resource_identity_role
        )

    def post_create_task(self) -> None:
        """ResourceIdentity post create task
        """
        self.resource.add_resouce_identity(self)
        self.identity.add_resource_identity(self)


class ResourceIdentityRole(BaseModel):
    """Model representing role assigned to an identity and resource.

    Attributes:
        resource_identity: ResourceIdentity instance
        role_constant: string representing role constant
        assigned_explicity: Is True when the role is not inherited and 
            is assigned explicity.
        source_group_identities: The GroupIdentity instances from which the role
            gets applied.
        inherited_from_resource_identities:: The parent resources via ResoureIdentity 
            representing the resource from which this role is being inherited.
    """

    collection_names = ('resource_identity_roles',)

    def __init__(
        self,
        resource_identity: ResourceIdentity,
        resource_role_constant: ResourceRoleConstant,
        assigned_explicitly: bool = True
    ):

        self.resource_identity: ResourceIdentity = resource_identity
        self.role_constant: ResourceRoleConstant = resource_role_constant

        self.assigned_explicitly = assigned_explicitly

        self._unique_id = f"{self.resource_identity.unique_id}_{self.role_constant}"

        self.source_group_identities: MutableMapping[
            GroupIdentityUID, GroupIdentity] = dict()

        self.inherited_from_resource_identities: MutableMapping[
            ResourceIdentityUID, ResourceIdentity] = dict()

    def add_source_group_identity(self, group_identity: GroupIdentity) -> None:

        self.source_group_identities.setdefault(
            group_identity.unique_id,
            group_identity
        )

    def add_inherited_from_resource_identity(
        self,
        inherited_from_resource_identity: ResourceIdentity
    ) -> None:

        self.inherited_from_resource_identities.setdefault(
            inherited_from_resource_identity.unique_id,
            inherited_from_resource_identity
        )

    def post_create_task(self) -> None:
        """ResourceIdentityRole post create task. Adds 
        backlink to dependent models
        """
        self.resource_identity.add_role(self)
        self.resource_identity.identity.add_resource_identity_role(self)

        if self.assigned_explicitly:
            self.inherit_role_group_member_identities()
            self.inherit_role_in_child_resource()

    def inherit_role_in_child_resource(self) -> None:
        """ResourceIdentityRole's optional PostCreateTask.
        When a role is assigned to an identity and resource then the same role to the
        identity to applied to all the children resource recursively by this method.
        """

        resource: Resource = self.resource_identity.resource

        def traverse_tree(resources: Mapping[ResourceUID, Resource]):
            for resource_key in resources:
                resource = resources[resource_key]

                resource_identity = ResourceIdentity.get_or_create(
                    resource,
                    self.resource_identity.identity
                )

                resource_identity_role = ResourceIdentityRole.get_or_create(
                    resource_identity,
                    self.role_constant,
                    assigned_explicitly=False
                )

                resource_identity_role.add_inherited_from_resource_identity(
                    resource_identity
                )

                if (resource.child_resources):
                    traverse_tree(resource.child_resources)

        traverse_tree(resource.child_resources)

    def inherit_role_group_member_identities(self) -> None:
        """ResourceIdentityRole's optional PostCreateTask
        If a role is assinged to identity of GroupIdentity type and  resource
        then the same role for a resource is applied to all other members in a group
        by this method.
        """

        identity = self.resource_identity.identity

        if not isinstance(identity, GroupIdentity):
            return

        resource = self.resource_identity.resource

        group_identity_members = identity.group_identity_members.values()

        for group_identity_member in group_identity_members:
            member_identity = group_identity_member.member

            resource_identity = ResourceIdentity.get_or_create(
                resource,
                member_identity
            )

            resource_identity_role = ResourceIdentityRole.get_or_create(
                resource_identity,
                self.role_constant,
                assigned_explicitly=False
            )

            resource_identity_role.add_source_group_identity(
                identity  # is a group_identity
            )

    @classmethod
    def add_instance_to_collection(
        cls,
        collection: MutableMapping[BaseModelUID, DerivedClass],
        instance_obj: DerivedClass
    ) -> 'ResourceIdentityRole':
        """The explicitly assigned roles overrides the implicit roles
        if exists in a graph.

        Args:
            collection: Collection of roles
            instance_obj: The instace to be to be fetched or inserted 
                in a collection 
        """

        casted_instance_obj: ResourceIdentityRole = cast(
            ResourceIdentityRole, instance_obj)

        assigned_explicitly: bool = casted_instance_obj.assigned_explicitly

        instance = cast(
            ResourceIdentityRole, collection.setdefault(
                instance_obj.unique_id,
                instance_obj
            )
        )

        if assigned_explicitly:
            instance.assigned_explicitly = assigned_explicitly
        
        return instance
