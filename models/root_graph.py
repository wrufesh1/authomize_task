from typing import TYPE_CHECKING

from collections.abc import (
    MutableMapping
)

if TYPE_CHECKING:
    from .identity import (
        Identity,
        IdentityUID, 
        UserIdentity,
        UserIdentityUID, 
        ServiceAccountIdentity,
        ServiceAccountUID, 
        GroupIdentity,
        GroupIdentityUID
    )

    from .resource import (
        Resource,
        ResourceUID,
        ResourceIdentity,
        ResourceIdentityUID,
        ResourceIdentityRole,
        ResourceIdentityRoleUID
    )

class RootGraph:
    """Class representing root node of graph.

    Attributes:
        identities: Collection of all identities available.
        user_identities: Collection of all identities of type USER.
        service_account_identities: Collection of all identities of type
            SERVICE_ACCOUNT
        group_identities: Collection of all identities of type GROUP
        resources: Collection of all available resource.
        resource_identities: Collection of all identity and resource relation.
        resource_identities_roles: Collection of all kind of roles assigned.
    """

    identities: MutableMapping[
        'IdentityUID', 'Identity'] = dict()

    user_identities: MutableMapping[
        'UserIdentityUID', 'UserIdentity'] = dict()

    service_account_identities: MutableMapping[
        'ServiceAccountUID', 'ServiceAccountIdentity'] = dict()

    group_identities: MutableMapping[
        'GroupIdentityUID', 'GroupIdentity'] = dict()

    resources: MutableMapping[
        'ResourceUID', 'Resource'] = dict()

    resource_identities: MutableMapping[
        'ResourceIdentityUID', 'ResourceIdentity'] = dict()

    resource_identity_roles: MutableMapping[
        'ResourceIdentityRoleUID', 'ResourceIdentityRole'] = dict()


graph = RootGraph()