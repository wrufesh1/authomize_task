from typing import Dict, Sequence
import unittest

import warnings

from main.parser import add_group_users_to_graph

from main.task import (
    built_graph,
    get_resouce_hierarchy,
    get_identity_roles,
    get_identity_and_role_by_resource,
    fetch_data_from_google_api,
    get_user_identity_roles,
    get_id_and_role_by_resource
)

from main.google_api_utils import (
    get_all_group_users
)

from models.root_graph import graph

class TaskTest(unittest.TestCase):
    """Lazy test cases
    """

    def setUp(self):
        built_graph()

        self.graph = graph

        warnings.simplefilter("ignore", ResourceWarning)

    
    def test_task_1(self):
        self.assertEqual(14, len(self.graph.resources))

    def test_task_2(self):
        
        test_resource = 'folders/93198982071'

        expected_output = ["folders/96505015065","folders/767216091627","organizations/1066060271767"]
        
        result = get_resouce_hierarchy(test_resource)

        self.assertEqual(result, expected_output)
    
    def test_task_3(self):

        result = get_identity_roles('user:ron@test.authomize.com')
        
        self.assertEqual(53, len(result))

    def test_task_4(self):

        test_resource = 'folders/93198982071'

        result = get_identity_and_role_by_resource(test_resource)

        self.assertEqual(14, len(result))

    def test_task_5(self):
        
        users, groups, group_users = fetch_data_from_google_api()

        self.assertEqual(
            (list, list, dict),
            (
                type(users), 
                type(groups), 
                type(group_users)
            )
        )

    def test_task_6(self):
        """Below is the user_groups schema
        
        user_groups_schema = {
            '<group id>': [
                '<user: id>',
                .....
            ]
        }

        End user_groups schmema

        'reviewers@test.authomize.com': this group has "roles/viewer" on folders/96505015065
        'ron@test.authomize.com': this user has no "roles/viewer" on folders/96505015065

        At the end i should get i should get "ron@test.authomize.com" with "roles/viewer" 
        on folders/96505015065        
        """
        user_groups = get_all_group_users()
        output = add_group_users_to_graph(user_groups)
        self.assertEqual(output, True)


        user_groups: Dict[str, Sequence[str]] = {
            'reviewers@test.authomize.com': [
                "ron@test.authomize.com",
            ]
        }


        add_group_users_to_graph(user_groups)

        roles = get_user_identity_roles(
            "user:ron@test.authomize.com"
        )

        required_role = ("folders/96505015065", "folders", "roles/viewer")

        self.assertEqual(required_role in roles, True)

        roles = get_id_and_role_by_resource( 
            "folders/96505015065"
        )

        required_role = ("user:ron@test.authomize.com", "roles/viewer")

        self.assertEqual(required_role in roles, True)

